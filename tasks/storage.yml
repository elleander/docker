---
- name: Check stat of docker_storage_dev
  stat:
    path: "/dev/{{docker_storage_dev}}"
    get_checksum: no
    get_md5: no
  register: r_docker_storage_dev_stat

- name: Assert that docker_storage_dev is block device and exist
  assert:
    msg: "Given docker_storage_dev doesn't exists"
    that:
    - r_docker_storage_dev_stat.stat.exists
    - r_docker_storage_dev_stat.stat.isblk

- include_tasks: detect-driver.yml
  when:
  - r_docker_storage_driver is undefined

- name: Check if upgrade required
  fail:
    msg: >
      Devicemapper detected, you need to set docker_storage_upgrade to true for upgrade to overlay2,
      This will stop docker daemon and remove docker file system (images, containers etc.)
  when:
  - r_docker_storage_driver == "devicemapper"
  - not docker_storage_upgrade | default(false)

- name: Create docker file
  template:
    src: docker-storage-setup.j2
    dest: "{{docker_storage_conf_file}}"
  register: r_docker_storage_setup_file
  notify: restart docker

- name: Create parition if docker_storage_dev is block device
  block:
    - parted:
        state: present
        device: "/dev/{{docker_storage_dev}}"
        number: 1
      register: r_docker_storage_parted
    - setup:
        filter: ansible_devices
      when: r_docker_storage_parted is changed
    - set_fact:
        docker_storage_pvs: |
          {{ansible_devices[docker_storage_dev]['partitions'].keys() | first}}
  when:
    - ansible_devices[docker_storage_dev] is defined

- name: Get pvs name
  set_fact:
    docker_storage_pvs: "{{docker_storage_dev}}"
  when:
    - ansible_devices[docker_storage_dev] is undefined

# those changes are destructive only do when upgrade or when new installation
- import_tasks: clear-storage.yml
  when:
  - >
    (r_docker_storage_driver == "empty") or
    (r_docker_storage_driver == "devicemapper") or
    (r_docker_installed is changed) or
    (r_docker_storage_setup_file is changed)

# those changes are not destructive (?)
- name: Create virtual group
  lvg:
    vg:  "{{docker_storage_vg}}"
    pvs: "/dev/{{docker_storage_pvs | trim}}"
  when:
  - ansible_lvm['vgs'][docker_storage_vg] is undefined
  notify: restart docker

- name: Create logical volume
  lvol:
    vg: "{{docker_storage_vg}}"
    lv: "{{docker_storage_lv}}"
    size: 100%FREE
  when:
  - ansible_lvm['lvs'][docker_storage_lv] is undefined
  notify: restart docker

- name: reset docker storage
  shell: docker-storage-setup --reset
  when:
  - r_docker_storage_cleared is defined
  - r_docker_storage_cleared

- name: call docker-storage-setup
  command: docker-storage-setup